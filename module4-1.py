

#1.Match the string "hello world" in a sentence  
# regex: [\w\s]*(hello world)+[\s\w]*

#2.Find all words in an input string that contain a triple vowel
# regex: \b\w*[aeiouAEIOU]{3}\w*\b

#3.Match an input string that is entirely a flight code, of the format AA####,
# regex: ^[A-Z][A-Z]\d{3,4}$

import re;

helloworld_regex = re.compile(r"[\w\s]*(hello world)+[\s\w]*");
def match_hello_world(test1):
	match = helloworld_regex.match(test1)
	if match is not None:
		return match.group(1)
		#return match.group(0)
	else:
		return False

print(match_hello_world("asdasdhello worldasdas"));
print(match_hello_world("hello worldasdas"));
print(match_hello_world("asdasdasdhello world"));
print(match_hello_world ("Programmers will often write hello world as their first project with a programming language."));

triplevowel_regex = re.compile(r"\b\w*[aeiouAEIOU]{3}\w*\b");
def find_all_3vowel(test2):
	#match = triplevowel.match(test2)
	return triplevowel_regex.findall(test2);
	#we need to call match() or findall() function over the regex object

print(find_all_3vowel("The gooey peanut butter and jelly sandwich was a beauty."));


flightcode_regex = re.compile(r"^[A-Z][A-Z]\d{3,4}$");
def match_flight_code(test3):
	match = flightcode_regex.match(test3)
	if match is not None:
		return match.group(0)
	else:
		return False
#the following output should be true
print( match_flight_code("AA1204") );
print( match_flight_code("AA312") );
#the following output shoulc be false
print( match_flight_code("NW314 and CO118") );
print( match_flight_code("US31344") );