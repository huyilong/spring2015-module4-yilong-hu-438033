import re
import sys, os
global find_name, find_atBats, find_hits, find_runs

find_name = None
find_atBats = None
find_hits = None
find_runs = None


class Player:
	# initialization for encapsulated data
	name = ""
	hits = 0
	atBats = 0 
	runs = 0
	avgBat = 0

	# constructor of player class
	def __init__(self, name):
		self.name = name

#define a function to find a certain player in a list
def find_player(people_list, name):
	for person in people_list:
		if person.name == name:
			return person

#check if the input from user is legal
if len(sys.argv) < 2:
	sys.exit("Usage: %s filePath" % sys.argv[0])
	filename = sys.argv[1]
#create an array for containing all the player objects
playerList = []

#check if the file exists in the given path
if not os.path.exists(sys.argv[1]):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

#read the lines in to a variable if the file exists
filename = sys.argv[1]
contents = [newLine.strip() for newLine in open(filename)]

#define the regex for finding the desired information in each line by groups
myRegex = re.compile(r"(\w+ \w+) batted (\d+) times with (\d) hits and (\d) runs")

for line in contents:
	if(line != '' and "=" not in line):
		#for each line in the contents we use the regex pattern to fetch information
		result = myRegex.match(line)
		#update the playerList with information if meet a line matched with regex
		if (result != None):
			find_name = result.group(1)
			find_atBats = int(result.group(2))
			find_hits = int(result.group(3))
			find_runs = int(result.group(4))
	        #create an object of player class by passing the name to constructor
	        if (find_name != None and find_atBats != None and find_hits != None and find_runs != None):
	        	if ( find_player(playerList, find_name) == None ):
	        		#the player is not in the list and thus we need to create a new one
	        		newPlayer = Player(find_name)
	        		newPlayer.hits = find_hits
	        		newPlayer.atBats = find_atBats
	        		newPlayer.runs = find_runs
		        	#add the new player to playerList
		        	playerList.append(newPlayer)

		        else:
	        		#the player already exists in the list so we only need to update his information
	        		updatePlayer = find_player(playerList, find_name)
	        		updatePlayer.hits += find_hits
	        		updatePlayer.atBats += find_atBats
	        		updatePlayer.runs += find_runs


#calculate and set the value of avgBat for each player in the list
for player in playerList:
	player.avgBat = round(float(player.hits)/float(player.atBats), 3)

#sort the playerlist by the avgBat in the Player class and according to a descending order
sortedList = sorted(playerList, key = lambda player : player.avgBat, reverse = True)

#print out the final sorted result of player list
for player in sortedList:
	print player.name,":",'{0:.3f}'.format(player.avgBat)



